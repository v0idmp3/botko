<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/facebook/{uuid}', 'Messenger\FacebookController@webhook');
Route::post('/facebook/{uuid}/save-token', 'Messenger\FacebookController@saveToken');
Route::post('/facebook/{uuid}', 'Messenger\FacebookController@parser');


Route::post('/telegram/{uuid}/save-token', 'Messenger\TelegramController@saveToken');
Route::post('/telegram/{uuid}', 'Messenger\TelegramController@parser');
Route::get('/telegram/test', 'Messenger\TelegramController@test');
