<?php


use App\Http\Controllers\Frontend\FrontController;
// Frontend routes

Route::get('', 'Frontend\FrontController@index');
Route::match(['get','post'], '/login', 'Frontend\FrontController@login');
Route::match(['get','post'], '/register', 'Frontend\FrontController@register');


