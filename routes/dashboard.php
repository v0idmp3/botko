<?php

//Dashboard routes

Route::get('/', 'Dashboard\DashboardController@index');
Route::group(['prefix' => 'platform'], function() {
    Route::get('facebook', 'Messenger\FacebookController@index');
    Route::get('telegram', 'Messenger\TelegramController@index');
});



Route::group(['prefix' => 'bots'], function() {
    Route::get('/', 'Dashboard\BotsController@index');
    Route::get('/my', 'Dashboard\BotsController@my');
    Route::post('renderBots', 'Dashboard\BotsController@renderBots');
    Route::post('add', 'Dashboard\BotsController@addBot');
    Route::post('remove', 'Dashboard\BotsController@removeBot');
    Route::post('help', 'Dashboard\BotsController@help');
    Route::get('/test', 'Messenger\TestController@test');
});

Route::group(['prefix' => 'apibots'], function() {
    Route::get('/', 'Dashboard\ApiBotsController@index');
    Route::post('/add', 'Dashboard\ApiBotsController@add');
    Route::match(['get', 'post'], 'edit/{id}', 'Dashboard\ApiBotsController@edit');
    Route::get('/delete/{id}', 'Dashboard\ApiBotsCOntroller@delete');
});




