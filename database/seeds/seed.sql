-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2016 at 08:01 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `wa_botko`
--

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
  (1, 'create-bot', 'Create bot', 'Permission for creating bots', '2016-10-09 15:07:17', '2016-10-09 15:07:17'),
  (2, 'dashboard', 'Dashboard', 'Have access to dashboard', '2016-10-09 16:08:11', '2016-10-09 16:08:11');



--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Site Admin', 'This role is only for admins', '2016-10-09 15:03:07', '2016-10-09 15:03:07'),
(2, 'free-user', 'Free User', 'This role is for free users', '2016-10-09 15:03:24', '2016-10-09 15:03:24'),
(3, 'paid-user', 'User', 'This role is for paid users', '2016-10-09 15:03:37', '2016-10-09 15:03:37'),
(4, 'banned', 'Banned', 'Banned user', '2016-10-09 16:09:33', '2016-10-09 16:09:33');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
  (1, 'e4eaaaf2-d142-11e1-b3e4-080027620cdd', 'ilija', 'v0idmp3@gmail.com', '$2y$10$VwcgXiVkv0AxrEAtRsXhMOCPpXGEAQ0m9ZFqKSxQcctviktSRvZHq', 'dMNwhy6lVOfG7JGhvM9DQFyLGVoxnar3W5C5X4nIfHzgnPB2KLJ968xOO3Pa', '2016-10-09 14:31:04', '2016-10-09 14:56:05');

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 2);

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
  (1, 1),
  (2, 1),
  (2, 2),
  (2, 3);

INSERT INTO `facebook_apps` (`uuid`, `verify_token`, `page_token`, `created_at`, `updated_at`) VALUES
('e4eaaaf2-d142-11e1-b3e4-080027620cdd', 'ovo_je_token', 'EAAKz1cX2LpsBANxSJZAkmMyuAm93wDGMTXqxPUxOZAJsh4iKm05ZClUQAQfiesJf3r26Ku3tRCwo4CMjVBwFsItoEsxgEJhsAjjcApZB5X9uGdoSkruRxQ0MU0ZBxIJENMM3qZBOEmZBJlrXlu2FvSHoxblBDihmsEjSTA7zaZAZCdwZDZD', '2016-10-09 14:31:04', '2016-10-09 14:56:05');

INSERT INTO `telegram_bots` (`uuid`, `access_token`, `created_at`, `updated_at`) VALUES
('e4eaaaf2-d142-11e1-b3e4-080027620cdd', '289966461:AAETtkc_qwKm_3HMTBHDr7CCVJ-XLcVdXAM', '2016-10-09 14:31:04', '2016-10-09 14:56:05');

INSERT INTO `bots` (`name`, `keywords`, `description`, `help`, `data`, `scope`, `created_at`, `updated_at`) VALUES ('Hi Bot', 'hi,hello,yo', 'This is default "Hi" bot', 'You can always summon help bot with typing "Hi", "Hello" or "Yo"', '{"class":"HiBot"}', 'system', '2016-10-20 10:27:25', '2016-10-20 10:27:25');
INSERT INTO `bots` (`name`, `keywords`, `description`, `help`, `data`, `scope`, `created_at`, `updated_at`) VALUES
('Wiki Bot', 'wiki,wikipedia', 'This is Wikipedia bot. With this bot you can search wikipedia', 'Use this bot with "wiki something"', '{"class":"WikiBot"}', 'system', '2016-10-20 10:27:25', '2016-10-20 10:27:25');
INSERT INTO `bots` (`name` ,`keywords` ,`description` ,`help` ,`data` ,`scope` ,`created_at` ,`updated_at`)
VALUES ('Search Bot',  'search,find',  'This is search bot. With this bot you can search for anything on Internet',  'To Use this bot, use "search ''something''"',  '{"class":"SearchBot"}',  'system',  '2016-10-20 12:27:25',  '2016-10-20 12:27:25');
INSERT INTO `bots` (`name`, `keywords`, `description`, `help`, `data`, `scope`, `created_at`, `updated_at`) VALUES ('Youtube Bot', 'youtube,tube,yt,video', 'This is YouTube Video Bot', 'To Use this bot, use "video ''something''"', '{"class":"YouTubeBot"}', 'system', '2016-10-20 12:27:25', '2016-10-20 12:27:25');



INSERT INTO `user_bots` (`uuid`, `bot_id`, `created_at`, `updated_at`) VALUES ('e4eaaaf2-d142-11e1-b3e4-080027620cdd', '1', '2016-10-20 16:38:39', '2016-10-20 16:38:39');

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'edit-bot', 'Edit bot', 'Permission for editing bots', NOW(), NOW());