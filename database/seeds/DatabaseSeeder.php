<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        Eloquent::unguard();

        $path = 'database/seeds/seed.sql';
        DB::unprepared(file_get_contents($path));
//        exec("mysql -u ".\Config::get('database.mysql.user')." -p ".\Config::get('database.mysql.password')." ".\Config::get('database.mysql.database')." < database/seeds/seed.sql");


    }
}
