<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserBots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->foreign('uuid')->references('uuid')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');           
            $table->integer('bot_id', false,true);
            $table->foreign('bot_id')->references('id')->on('bots')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bots');
    }
}
