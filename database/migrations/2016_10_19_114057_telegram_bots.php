<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TelegramBots extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('telegram_bots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->foreign('uuid')->references('uuid')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('access_token');
            $table->string('name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('telegram_bots');
    }

}
