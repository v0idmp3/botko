<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Bots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('bots', function (Blueprint $table) {
            $table->increments('id');
             $table->string('uuid', 36)->nullable();
            $table->foreign('uuid')->references('uuid')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->text('keywords');
            $table->text('description');
            $table->text('help');
            $table->longText('data');
            $table->enum('scope', ['public','private', 'system'])->default('private');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('bots');
    }
}
