<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FacebookApp extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('facebook_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid', 36);
            $table->foreign('uuid')->references('uuid')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->string('verify_token');
            $table->string('page_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('facebook_apps');
    }

}
