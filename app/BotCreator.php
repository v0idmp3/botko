<?php

namespace App;

use App\BotInterface\BotCreatorInterface;
use App\BotInterface\BotInterface;

class BotCreator implements BotCreatorInterface {

    protected $keyword;
    protected $rest;
    protected $bots;
    protected $platform;
    protected $class;
    protected $bot_id;
    protected $bang;

    public function __construct($keyword, $rest, $bots, $platform, $bang = false) {        
        $this->setKeyword($keyword);
        $this->setRest($rest);
        $this->setBots($bots);
        $this->setPlatform($platform);
        $this->bang = $bang;
        $this->findBot();
        $this->createBot();
    }

    public function createBot(): BotInterface {
        if ($this->class != "") {
            $class = "App\\Bots\\" . $this->class;
        } else {
            $class = "App\\Bots\\DefaultBot";
        }

        if ($this->bang) {
            return new Bots\SearchBot($this);
        } else {
            return new $class($this);
        }
    }

    public function findBot() {
        foreach ($this->bots as $bot) {
            $keywords = explode(",", $bot->bot->keywords);
            if (in_array($this->keyword, $keywords)) {
                $data = json_decode($bot->bot->data, true);
                $this->class = $data['class'];
                $this->bot_id = $bot->bot_id;
                break;
            }
        }
    }

    public function setKeyword($keyword) {
        $this->keyword = $keyword;
    }

    public function setRest($rest) {
        $this->rest = $rest;
    }

    public function setBots($bots) {
        $this->bots = $bots;
    }
    
    public function setPlatform($platform){
        $this->platform = $platform;
    }

    public function getKeyword(){
        return $this->keyword;
    }
    
    public function getBotId(){
        return $this->bot_id;
    }
    
    public function getRest(){
        return $this->rest;
    }
    
    public function getBots(){
        return $this->bots;
    }
    
    public function getBang(){
        return $this->bang;
    }
    
    public function getPlatform(){
        return $this->platform;
    }
}
