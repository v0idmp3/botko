<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
//        dd($this->app->environment());
        if ($this->app->environment() == 'local') {
            // This enable the debugbar
            \URL::forceSchema('https');
            $this->app['config']->push('debugbar.enabled', true);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
