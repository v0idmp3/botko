<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DashboardProtection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if(!Auth::check()){
            return redirect('/login')->with(['message' => 'You must login', 'alert-class' => 'alert-danger']);
        }
        if (Auth::check() && !Auth::user()->can('dashboard')) {
            return redirect('/login')->with(['message' => 'You dont have permission to access dashboard', 'alert-class' => 'alert-danger']);
        }

        return $next($request);
    }
}
