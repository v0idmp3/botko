<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Bots;
use App\Models\UserBots;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiBotsController extends Controller {

    public function index() {
        $bots = Bots::where('uuid', '=', Auth::user()->uuid)->get();
        return $this->render('dashboard.pages.apibot-index', ['bots' => $bots]);
    }

    public function add(Request $request) {
        $rules = [
            'name' => 'required|min:3',
            'keywords' => 'required',
            'url' => 'required|url',
            'request' => 'required',
            'error' => 'required',
            'param_value.*' => 'required_with:param_name.*'
        ];
        $messages = [];


        $keywords = explode(",", strtolower(str_replace(" ", ",", $request->get('keywords'))));

        $bots = new Bots();
        $usedKeyword = $bots->searchBotsByKeyword($keywords, Auth::user()->uuid);
        if ($usedKeyword) {
            $rules['used_keyword'] = 'required';
            $messages['used_keyword.required'] = 'Keyword "' . $usedKeyword . '" is already in use';
        }

        $name = $request->get('name');
        $usedName = $bots->searchBotsByName($name, Auth::user()->uuid);
        if ($usedName) {
            $rules['used_name'] = 'required';
            $messages['used_name.required'] = 'Name "' . $name . '" is already in use';
        }

        $validator = \Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->getMessageBag();            
            $messagesData = array();
            if ($messages->get('param_value.0')) {
                for ($i = 0; $i < sizeof($messages->all()) - 3; $i++) {

                    $messagesData['param_value'][] = $messages->get('param_value.' . $i)[0];
                }
                $messages->merge($messagesData);
            }


            return redirect('dashboard/apibots')
                            ->withErrors($validator)
                            ->withInput();
        }

        $params = [];
        $param_names = $request->get('param_name');

        $param_values = $request->get('param_value');
        for ($i = 0; $i < sizeof($param_names); $i++) {
            if ($param_names[$i] != "") {
                $params[$param_names[$i]] = $param_values[$i];
            }
        }

        $data = [
            'class' => 'ApiBot',
            'url' => $request->get('url'),
            'type' => $request->get('request'),
            'params' => $params,
            'error' => $request->get('error')
        ];

        $keywords = array_filter($keywords, function($value) {
            return $value !== '';
        });
        $bots->uuid = Auth::user()->uuid;
        $bots->name = $name;
        $bots->keywords = implode(",", $keywords);
        $bots->description = $request->get('description');
        $bots->help = $request->get('help');
        $bots->data = json_encode($data);
        $bots->scope = Bots::SCOPE_PRIVATE;
        $bots->save();

        $userBots = new UserBots();
        $userBots->uuid = Auth::user()->uuid;
        $userBots->bot_id = $bots->id;
        $userBots->save();
        return redirect('dashboard/apibots')->with("success", true);
    }

    public function edit($id, Request $request) {
        if ($request->getMethod() == "GET") {
            $bots = Bots::where('uuid', '=', Auth::user()->uuid)->get();
            $bot = Bots::where('id', '=', $id)->first();
            return $this->render('dashboard.pages.apibot-edit', ['bots' => $bots, 'bot' => $bot]);
        }
        $rules = [
            'name' => 'required|min:3',
            'keywords' => 'required',
            'url' => 'required|url',
            'request' => 'required',
            'error' => 'required',
            'param_value.*' => 'required_with:param_name.*'
        ];
        $messages = [];


        $keywords = explode(",", strtolower(str_replace(" ", ",", $request->get('keywords'))));

        $bots = new Bots();
        $usedKeyword = $bots->searchBotsByKeyword($keywords, Auth::user()->uuid, $id);
        if ($usedKeyword) {
            $rules['used_keyword'] = 'required';
            $messages['used_keyword.required'] = 'Keyword "' . $usedKeyword . '" is already in use';
        }

        $name = $request->get('name');
        $usedName = $bots->searchBotsByName($name, Auth::user()->uuid, $id);
        if ($usedName) {
            $rules['used_name'] = 'required';
            $messages['used_name.required'] = 'Name "' . $name . '" is already in use';
        }

        $validator = \Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $messages = $validator->getMessageBag();
            $messagesData = array();
            for ($i = 0; $i < sizeof($messages->all()) - 3; $i++) {
                $messagesData['param_value'][] = $messages->get('param_value.' . $i)[0];
            }
            $messages->merge($messagesData);

            return redirect('dashboard/apibots/edit/' . $id)
                            ->withErrors($validator)
                            ->withInput();
        }

        $params = [];
        $param_names = $request->get('param_name');

        $param_values = $request->get('param_value');
        for ($i = 0; $i < sizeof($param_names); $i++) {
            if ($param_names[$i] != "") {
                $params[$param_names[$i]] = $param_values[$i];
            }
        }

        $data = [
            'class' => 'ApiBot',
            'url' => $request->get('url'),
            'type' => $request->get('request'),
            'params' => $params,
            'error' => $request->get('error')
        ];

        $keywords = array_filter($keywords, function($value) {
            return $value !== '';
        });
        $bots = $bots->where('id', '=', $id)->first();
        $bots->uuid = Auth::user()->uuid;
        $bots->name = $name;
        $bots->keywords = implode(",", $keywords);
        $bots->description = $request->get('description');
        $bots->help = $request->get('help');
        $bots->data = json_encode($data);
        $bots->scope = Bots::SCOPE_PRIVATE;
        $bots->update();

        return redirect('dashboard/apibots/edit/' . $id)->with("success", true);
    }

    public function delete($id) {
        $bots = new Bots();
        $bots->where('id', '=', $id)->delete();
        return redirect('dashboard/apibots');
    }

}
