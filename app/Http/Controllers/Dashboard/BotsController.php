<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Bots;
use App\Models\UserBots;
use Auth;
use Illuminate\Http\Request;

class BotsController extends Controller {

    // render index page
    public function index() {                
        return $this->render('dashboard.pages.bots');
    }

    // render my bots page
    public function my() {
        return view('dashboard.pages.myBots');
    }

    // render sub-pages called from ajax for pages "myBots" and "bots"
    public function renderBots(Request $request) {
        $type = $request->get('type');
        if ($type == "registered") {
            $registeredBots = [];
            $userBots = new UserBots();
            $result = $userBots->getBots(Auth::user()->uuid);
            if ($result) {
                foreach ($result as $bot) {
                    $registeredBots[] = $bot->bot;
                }
            }
            return view('dashboard.pages.ajax.registeredBots')->with('registeredBots', $registeredBots);
        }
        if ($type == "available") {
            $availableBots = [];
            $registeredBots = [];
            $userBots = new UserBots();
            $result = $userBots->getBots(Auth::user()->uuid);
            if ($result) {
                foreach ($result as $bot) {
                    $registeredBots[] = $bot->bot;
                }
            }

            $bots = new Bots();
            $result = $bots->getBots(Auth::user()->uuid, $bots::SCOPE_PUBLIC, $registeredBots);
            if ($result) {
                foreach ($result as $bot) {
                    $availableBots[] = $bot;
                }
            }
            return view('dashboard.pages.ajax.availableBots')->with('availableBots', $availableBots);
        }
    }

    public function addBot(Request $request) {
        $bot_id = $request->get('bot_id');

        $userBotModel = new UserBots();

        if (!$userBotModel->checkByBot($bot_id)) {
            return 0;
        } else {
            $result = $userBotModel->addBot($bot_id);
            return 1;
        }
    }

    public function removeBot(Request $request) {
        $bot_id = $request->get('bot_id');

        $userBotModel = new UserBots();

        if ($userBotModel->checkByBot($bot_id)) {
            return 0;
        } else {
            $result = $userBotModel->deleteBot($bot_id);
            return 1;
        }
    }

    public function help(Request $request) {
        $id = $request->get('bot_id');
        $bot = Bots::where('id', '=', $id)->first();
        return json_encode($bot);
    }

}
