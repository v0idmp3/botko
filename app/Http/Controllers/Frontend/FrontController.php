<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class FrontController extends Controller{

    public function index(){
        return $this->render('frontend.pages.index');
    }

    public function login(Request $request){
        // Check if GET request is triggered
        if($request->isMethod('get')){
            return $this->render('frontend.pages.login');
        }

        $validator = \Validator::make($request->all(), [
            'email' =>  'required|email',
            'password'  =>  'required|min:6'
        ]);

        if($validator->fails()){
            return redirect('login')
                ->withErrors($validator)
                ->withInput();
        }

        // Auth attempt
        $remember = $request->get('remember_me')?$request->get('remember_me'):false;

        if(\Auth::attempt(['email'  =>  $request->get('email'), 'password'  =>  $request->get('password')], $remember)){
            return redirect('/dashboard');
        }else{
            return redirect('/login')->withInput()->with(['message' => 'Wrong login credentials.', 'alert-class' => 'alert-error']);
        }

    }

    public function register(Request $request){
        // Check if GET request is triggered
        if($request->isMethod('get')) {
            return $this->render('frontend.pages.register');
        }

        $validator = \Validator::make($request->all(), [
            'name'  =>  'required|min:4|max:255',
            'email' =>  'required|unique:users|email',
            'password'  =>  'required|min:6|confirmed',
            'password_confirmation' =>  'required|min:6',
            'tos'   =>  'required'
        ]);

        if($validator->fails()){
            return redirect('register')
                ->withErrors($validator)
                ->withInput();
        }

        // Save new user
        $user = new User($request->all());
        $user->password = Hash::make($request->get('password'));
        try{
            $user->uuid = Uuid::uuid4()->toString();
        }catch (UnsatisfiedDependencyException $e){
            return redirect('register')->withInput()->with(['message' => $e->getMessage(), 'alert-class' => 'alert-error']);
        }


        // Free user role id is
        $user->save();
        $user->attachRole(2);
        return redirect('/login')->with(['message' => 'Successful registration. Please login.', 'alert-class' => 'alert-success']);
    }




}