<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Models\TelegramBots;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Validator;

class TelegramController extends Controller {

    public function index() {
        $uuid = \Auth::user()->uuid;
        $telegramModel = new TelegramBots();
        $result = $telegramModel->getBot($uuid);

        if ($result) {
            return $this->render('dashboard/pages/platforms_telegram')->with(['access_token' => $result->access_token]);
        } else {
            return $this->render('dashboard/pages/platforms_telegram');
        }
    }

    public function saveToken($uuid, Request $request) {
        $validator = Validator::make($request->all(), [
                    'access_token' => 'required|min:4',
        ]);
        if ($validator->fails()) {
            $data = [
                'success' => false,
                'message' => "Please enter your access token",
                'input' => 'has-error has-feedback'
            ];
            return json_encode($data);
        }

        $token = $request->get('access_token');

        $fbModel = new TelegramBots();
        $fbModel->saveToken($uuid, $token);

        //Set telegram webhook
        $client = new Client();
        $request = $client->get('https://api.telegram.org:443/bot' . $token . '/setwebhook?url=' . \URL::to('/api/telegram/' . $uuid));

        $data = [
            'success' => true,
            'access_token' => $token
        ];
        return json_encode($data);
    }

    public function parser($uuid, Request $request) {
        $update = $request->json()->all();

        $client = new Client(['base_uri' => 'https://api.telegram.org/bot289966461:AAETtkc_qwKm_3HMTBHDr7CCVJ-XLcVdXAM/']);

        $command = $request->json('message.text');
        $router = new \App\MainRouter($uuid, 'telegram');
        $bot = $router->init($command)->createBot();

        $response = $bot->render();

        if (is_array($response)) {
            foreach ($response as $message) {
                $client->post('sendMessage', ['query' => [
                        'chat_id' => $request->json('message.chat.id'),
                        'text' => $message
                ]]);
            }
        } else {
            $client->post('sendMessage', ['query' => [
                    'chat_id' => $request->json('message.chat.id'),
                    'text' => $response
            ]]);
        }
    }

}
