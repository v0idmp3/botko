<?php

namespace App\Http\Controllers\Messenger;

use App\Http\Controllers\Controller;
use App\Models\FacebookApps;
use Auth;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Validator;

class FacebookController extends Controller {

    public function index() {
        $uuid = Auth::user()->uuid;
        $fbModel = new FacebookApps();
        $result = $fbModel->getApp($uuid);

        if ($result) {
            return $this->render('dashboard/pages/platforms_facebook')->with(['verify_token' => $result->verify_token, 'page_token' => $result->page_token]);
        } else {
            return $this->render('dashboard/pages/platforms_facebook');
        }
    }

    public function saveToken($uuid, Request $request) {
        $validator = Validator::make($request->all(), [
                    'verify_token' => 'required|min:4',
                    'page_token' => 'required'
        ]);
        if ($validator->fails()) {
            $data = [
                'success' => false,
                'message' => "Please enter your verify token and/or page_token",
                'input' => 'has-error has-feedback'
            ];
            return json_encode($data);
        }

        $token = $request->get('verify_token');
        $page_token = $request->get('page_token');


        $fbModel = new FacebookApps();
        $fbModel->saveToken($uuid, $token, $page_token);

        $data = [
            'success' => true,
            'verify_token' => $token,
            'page_token' => $page_token
        ];
        return json_encode($data);
    }

    public function webhook($uuid, Request $request) {
        $fbModel = new FacebookApps();
        $result = $fbModel->getApp($uuid);
        if ($result) {
            $verify_token = $result->verify_token;
            $page_token = $result->page_token;
            $hub_verify_token = null;

            if ($request->has('hub_challenge')) {
                $challenge = $request->get('hub_challenge');
                $hub_verify_token = $request->get('hub_verify_token');
            }


            if ($hub_verify_token === $verify_token) {
                echo $challenge;
                die();
            }
        } else {
            echo "Something is wrong!!!";
        }
    }

    public function parser($uuid, Request $request) {
//        $request = $request->all();
        $sender = $request->json('entry.0.messaging.0.sender.id');
        $command = $request->json('entry.0.messaging.0.message.text');

        //send replay
        $fbModel = new FacebookApps();
        $result = $fbModel->getApp($uuid);

        $router = new \App\MainRouter($uuid, 'facebook');
        $bot = $router->init($command)->createBot();


        $response = $bot->render();

        $client = new Client();

        if (is_array($response)) {
            foreach ($response as $message) {
                $client->post("https://graph.facebook.com/v2.6/me/messages", ['query' => [
                        'access_token' => $result->page_token,
                        'recipient' => ['id' => $sender],
                        'message' => [
                            'text' => $message
                        ]
                ]]);
            }
        } else {
            $client->post("https://graph.facebook.com/v2.6/me/messages", ['query' => [
                    'access_token' => $result->page_token,
                    'recipient' => ['id' => $sender],
                    'message' => [
                        'text' => $response
                    ]
            ]]);
        }
    }

}
