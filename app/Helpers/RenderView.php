<?php


namespace App\Helpers;

use Illuminate\Support\Facades\View;

trait RenderView {

    public function render($view, array $data = null){

        \View::share($data);

        return view($view);
    }
}