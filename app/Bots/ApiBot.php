<?php

namespace App\Bots;

use App\BotInterface\BotInterface;
use App\BotTraits\BotTrait;
use App\Models\Bots;

class ApiBot implements BotInterface {

    use BotTrait;

    public function __construct(\App\BotCreator $creator) {
        $this->keyword = $creator->getKeyword();
        $this->rest = $creator->getRest();
        $this->bots = $creator->getBots();
        $this->id = $creator->getBotId();
        $this->bot = Bots::where('id', '=', $this->id)->first();        
        $this->think();
    }
    
     public function think() {
        $data = json_decode($this->bot->data, true);
        $params = $data['params'];
        $url = $data['url'];
        $type = $data['type'];
        $client = new \GuzzleHttp\Client();
        
        try{
            
            if(substr($url, -1) == "/"){
                $url = substr($url, 0, -1);
            }  
            
            if($type == "get"){          
                $paramString = "";
                if(sizeof($params) > 0){
                    foreach($params as $key => $value){
                        $paramString .= "/".$key."/".$value;
                    }
                }
                $response = $client->request($data['type'], $url.$paramString);     
            }else if($type == "post"){
                $response = $client->request($data['type'], $url); 
            }   
            
            if($response->getStatusCode() == 200){                
                $data = json_decode($response->getBody()->getContents(), true);
            }else if($response->getStatusCode() == 201){
                $data = json_decode($response->getBody()->getContents(), true);                
            }
            
            if(isset($data['response'])){
                $this->response = $data['response'];
            }else{
                $data = json_decode($this->bot->data, true);               
                $this->response = $data['error'];                
            }
            return $this;
            
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
            if ($response && $response->getStatusCode() == 200) {
                 
            }
            else {
                $data = json_decode($this->bot->data, true);               
                $this->response = $data['error'];
                return $this;
            }
        }                
    }
}