<?php

namespace App\Bots;

use App\BotInterface\BotInterface;
use App\BotTraits\BotTrait;
use App\Models\Bots;

class SearchBot implements BotInterface {

    use BotTrait;

    protected $bang;

    public function __construct(\App\BotCreator $creator) {
        $this->keyword = $creator->getKeyword();
        $this->rest = $creator->getRest();
        $this->bots = $creator->getBots();
        $this->id = $creator->getBotId();
        $this->bot = Bots::where('id', '=', $this->id)->first();
        $this->bang = $creator->getBang();
        $this->think();
    }

    public function think() {
        if ($this->bang) {
            $this->withBang();
        } else {
            $this->withoutBang();
        }

        return $this;
    }

    protected function withBang() {
        $client = new \GuzzleHttp\Client();
        $this->rest = str_replace(' ', '+', $this->rest);
        $request = $client->request('GET', 'https://api.duckduckgo.com/?q=!' . $this->keyword . '+' . $this->rest . '&format=json&pretty=1&no_redirect=1');

        if ($request->getStatusCode() == 200) {
            $response = $request->getBody();
            $response = json_decode($response, true);
            if (strlen($response['Redirect']) > 0) {
                $this->response = $response['Redirect'];
            } else {
                $this->response = $this->defaultError;
            }
        }
    }

    protected function withoutBang() {
        $this->response = "This bot is still WIP";
        return $this;
//        $headers = [
//            'Connection' => 'keep-alive',
//            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
//            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
//        ];
//        $start_time = time();
//        $client = new \GuzzleHttp\Client(['headers' => $headers]);
//        $request = $client->get(
//                'https://duckduckgo.com/html/?q=' . $this->rest, [
//                ]
//        );
//        if ($request->getStatusCode() == 200) {
//            $response = $request->getBody();
//            $content = $response->getContents();
//            $end_time = time() - $start_time;
//            dd($end_time);
//        }
    }

}
