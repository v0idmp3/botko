<?php

namespace App\Bots;

use App\BotTraits\BotTrait;
use App\BotInterface\BotInterface;

class DefaultBot implements BotInterface{
    
    use BotTrait;
    
    public function render(): string {
        return "This command is unrecognize. Please type \".help\" to see available commands";
    }

    public function __construct(\App\BotCreator $creator) {      
        dd($creator->getPlatform());
    }

    public function think() {        
    }


}