<?php

namespace App\Bots;

use App\BotInterface\BotInterface;
use App\BotTraits\BotTrait;
use App\Models\Bots;

class WikiBot implements BotInterface {

    use BotTrait;

    public function __construct(\App\BotCreator $creator) {
        $this->keyword = $creator->getKeyword();
        $this->rest = $creator->getRest();
        $this->bots = $creator->getBots();
        $this->id = $creator->getBotId();
        $this->bot = Bots::where('id', '=', $this->id)->first();
        $this->think();
    }

    public function think() {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(
                'https://en.wikipedia.org/w/api.php', [
            'query' => [
                'action' => 'opensearch',
                'search' => $this->rest,
                'format' => 'json',
                'limit' => 1,
                'namespace' => 0
            ]
                ]
        );
        if ($request->getStatusCode() == 200) {
            $response = $request->getBody();
            $response = json_decode($response, true);

            if (isset($response[3][0])) {
                $result = $response[3][0];
            } else {
                $result = $this->defaultError;
            }

            $this->response = $result;
        } else {
            $response = $this->defaultError;
            $this->response = $response;
        }
        return $this;
    }

}
