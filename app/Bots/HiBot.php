<?php

namespace App\Bots;

use App\BotInterface\BotInterface;
use App\BotTraits\BotTrait;
use App\Models\Bots;

class HiBot implements BotInterface{
    
    use BotTrait;
    
    public function __construct(\App\BotCreator $creator) {
        $this->keyword = $creator->getKeyword();
        $this->rest = $creator->getRest();
        $this->bots = $creator->getBots();
        $this->id = $creator->getBotId();
        $this->bot = Bots::where('id', '=', $this->id)->first(); 
        $this->think();
    }        
       
    public function think(){
        $this->response = "Hello and welcome to Botko";
        return $this;
    }

}