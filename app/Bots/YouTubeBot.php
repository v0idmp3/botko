<?php

namespace App\Bots;

use App\BotInterface\BotInterface;
use App\BotTraits\BotTrait;
use App\Models\Bots;

class YouTubeBot implements BotInterface {

    use BotTrait;

    public function __construct(\App\BotCreator $creator) {
        $this->keyword = $creator->getKeyword();
        $this->rest = $creator->getRest();
        $this->bots = $creator->getBots();
        $this->id = $creator->getBotId();
        $this->bot = Bots::where('id', '=', $this->id)->first();        
        $this->think();
    }



    public function think() {
        $youtube = new \Alaouy\Youtube\Youtube('AIzaSyCLUzwrGpGLYeIDhpodetTzi2RVqpfHNcc');
        
        $results = $youtube->search($this->rest);
        
        if(!$results){
            $this->response = "BLAAA";
            return $this;
        }
        
        foreach ($results as $video) {
            if (isset($video->id->{'videoId'})) {
                $this->response[] = 'https://www.youtube.com/watch?v=' . $video->id->{'videoId'};
            } else if (isset($video->id->{'channelId'})) {
                $this->response[] = 'https://www.youtube.com/channel/' . $video->id->{'channelId'};
            }
        }
        return $this;
    }

    

}
