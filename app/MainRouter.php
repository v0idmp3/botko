<?php

namespace App;

use App\Models\UserBots;
use Auth;

class MainRouter {

    private $uuid;
    
    private $platform;

    public function __construct($uuid, $platform) {
        $this->uuid = $uuid;
        $this->platform = $platform;
    }

    public function init($text) {
        $checkKeyword = $this->isKeyword($text);
        if (!$checkKeyword) {
            return false;
        } else {
            $keyword = $checkKeyword['keyword'];
            $rest = $checkKeyword['rest'];
            $bots = $this->findRegisteredBots();
            if ($bots) {
                if (isset($checkKeyword['bang']) && $checkKeyword['bang']) {
                    return new BotCreator($keyword, $rest, $bots, $this->platform, $checkKeyword['bang']);
                } else {
                    return new BotCreator($keyword, $rest, $bots, $this->platform);
                }
            } else {
                return false;
            }
        }
    }

    /*
     * Keyword text start with DOT "." and end with WHITESPACE " "
     */

    protected function isKeyword($text) {
        $firstChar = $text[0]; //get first char in string

        $text = strtolower($text); //make lowercase
        // Check if user called command direct
        if ($firstChar == ".") {
            $keyword = explode(' ', trim($text));
            $keyword = str_replace('.', '', $keyword);
            $data['keyword'] = $keyword[0];
            //delete dot from input text;
            $text = str_replace('.', '', $text);
        } elseif ($firstChar == "!") {
            $keyword = explode(' ', trim($text));
            $keyword = str_replace('!', '', $keyword);
            $data['keyword'] = $keyword[0];
            //delete dot from input text;
            $text = str_replace('!', '', $text);
            $data['bang'] = true; // this is for duckduckgo BANG
        } else {
            $keyword = explode(' ', trim($text));
            $data['keyword'] = $keyword[0];
        }

        $text = str_replace($data['keyword'], '', $text); // remove keyword from string
        $data['rest'] = ltrim($text); // remove whitespace from begginig of string (if exist)        
        return $data;
    }

    /*
     * Find registered bots from user
     */

    protected function findRegisteredBots() {
        $model = new UserBots();
        return $model->getBots($this->uuid);
    }

}

/*
 * Keywords with dots are using only for custom user bots
 * Other bots have predefined keywords
 *
 * 1st Solution:
 * isKeyword() function first take first word from string and then
 *
 */