<?php

namespace App\BotTraits;

trait BotTrait{
    private $keyword;
    
    private $rest;
    
    private $bots;
    
    private $id;
    
    private $bot;
    
    private $defaultError = "Ups...Not found";
    
    private $response;
    
    public function getKeyword() {
        return $this->keyword;
    }

    public function getRest() {
        return $this->rest;
    }

    public function getBots() {
        return $this->bots;
    }

    public function getId() {
        return $this->id;
    }

    public function getBot() {
        return $this->bot;
    }

    public function setKeyword($keyword) {
        $this->keyword = $keyword;
    }

    public function setRest($rest) {
        $this->rest = $rest;
    }

    public function setBots($bots) {
        $this->bots = $bots;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setBot($bot) {
        $this->bot = $bot;
    } 

    
    public function help() {           
        if(isset($this->rest[0]) && $this->rest[0] == "-" && substr($this->rest, 1, 4) == "help"){
            $this->response = $this->bot->help;            
        }
        return $this;
    }
    
    public function render() {  
        $this->help();
        return $this->response;
    }
}