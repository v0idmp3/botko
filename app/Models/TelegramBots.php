<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelegramBots extends Model {

    protected $fillable = [
        'uuid',
    ];

    public function getBot($uuid) {
        return $this->where('uuid', '=', $uuid)->first();
    }

    public function saveToken($uuid, $token) {
        $result = $this->where('uuid', '=', $uuid)->first();
        if ($result) {
            //update
            $result->access_token = $token;
            $result->save();
        } else {
            //new
            $this->uuid = $uuid;
            $this->access_token = $token;
            $this->save();
        }
    }

}
