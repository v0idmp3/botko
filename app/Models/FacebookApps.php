<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacebookApps extends Model {

    protected $fillable = [
        'uuid', 'user_access_token',
    ];

    public function getApp($uuid) {
        return $this->where('uuid', '=', $uuid)->first();
    }

    public function saveToken($uuid, $token, $page_token) {
        $result = $this->where('uuid', '=', $uuid)->first();
        if ($result) {
            //update
            $result->verify_token = $token;
            $result->page_token = $page_token;
            $result->save();
        } else {
            //new
            $this->uuid = $uuid;
            $this->verify_token = $token;
            $this->page_token = $page_token;
            $this->save();
        }
    }

}
