<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bots extends Model {

    const SCOPE_PUBLIC = 'public';
    const SCOPE_PRIVATE = 'private';
    const SCOPE_SYSTEM = 'system';
    
    
    protected $fillable = [
        'uuid', 'name', 'keyword', 'description', 'help', 'data', 'scope'
    ];

    /**
     * 
     * @param type $uuid uuid for user who created bot
     * @param type $scope private or public
     * @param array $without IDs for already used bots
     * @return type
     */
    public function getBots($uuid, $scope, array $without){
        $query = $this->where(function($q) use ($uuid, $scope){
                $q->where('uuid', '=', $uuid)
                    ->orWhere('scope', '=', $scope)
                    ->orWhere('scope', '=', $this::SCOPE_SYSTEM);
        });                                                
        
        // except already used bots
        if(!empty($without)){            
            foreach($without as $bot){
                $query->where('id', '!=', $bot->id);
            }
           
        }
        $result = $query->get();
        
        
        return sizeof($result) > 0 ? $result : false;       
    } 
    
    /**
     * 
     * @param type $keywords
     * @param type $uuid
     * @return mixed
     */
    public function searchBotsByKeyword($keywords, $uuid, $id = false){
        foreach($keywords as $keyword){
            if($keyword != ""){
                $count = $this->where('keywords', 'LIKE', "%".$keyword."%");
                if($id){                    
                    $count->where('id', '!=', "$id");
                }
                $count->where(function($q) use ($uuid){
                            $q->where('uuid', '=', $uuid)
                                ->orWhere('scope', '=', $this::SCOPE_SYSTEM);
                    });
                    
               
                $count = $count->count();               
                if($count > 0){
                    return $keyword;
                }
            }
        }
        return false;
    }
    
    public function searchBotsByName($name, $uuid, $id=false){       
        $count = $this->where('name', 'LIKE', $name);
                if($id){
                    $count->where('id', '!=', $id);
                }
                $count->where(function($q) use ($uuid){
                    $q->where('uuid', '=', $uuid)
                        ->orWhere('scope', '=', $this::SCOPE_SYSTEM);
            });
            
        $count = $count->count(); 
        if($count > 0){
            return true;
        }         
        return false;
    }
    
}
