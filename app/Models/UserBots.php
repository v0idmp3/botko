<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBots extends Model {
    
    protected $fillable = [
        'uuid', 'bot_id'
    ];
    
    public function bot(){
        return $this->belongsTo('App\Models\Bots', 'bot_id');
    }
    
    public function user(){
        return $this->belongsTo('App\Models\User', 'uuid');
    }
    
    public function getBots($uuid){
        $result = $this->where('uuid', '=', $uuid)->get();
        return sizeof($result) > 0 ? $result : false;
    }
    
    public function checkByBot($id){
        $result = $this->where('bot_id', '=', $id)->where('uuid', '=', \Auth::user()->uuid)->count();        
        return $result > 0 ? false : true;
    }
    
    public function addBot($id){
        if($this->checkByBot($id)){
            $this->bot_id = $id;
            $this->uuid = \Auth::user()->uuid;
            $this->save();
            return true;
        }else{
            return false;
        }
    }
    
    public function deleteBot($id){
        $this->where('bot_id', '=', $id)->delete();
        return true;
    }
}