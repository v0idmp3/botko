<?php

namespace App\BotInterface;

interface BotCreatorInterface{
    
    public function setKeyword($command);
    
    public function setRest($rest);
    
    public function setBots($bots);
    
    public function findBot();
    
    public function createBot() : BotInterface;
    
}