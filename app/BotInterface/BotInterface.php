<?php

namespace App\BotInterface;

interface BotInterface {

    /**
     * Constructor
     */
    public function __construct(\App\BotCreator $creator);

    /**
     * This is main function for render result from bot
     */
    public function render();

    /**
     * This is main function for bot operation
     */
    public function think();
    
    /**
     * This is main function for rendering help text
     */
    public function help();
}
