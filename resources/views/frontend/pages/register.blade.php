@extends('frontend.layout')


@section('content')
    <h1>Register form:</h1>
    <form method="post" action="{{ URL::to('register') }}">
        {{ csrf_field() }}
        <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control " id="name" placeholder="Name" value="{{ old('name') }}">
            @if($errors->has('name'))
                @foreach ($errors->get('name') as $message)
                    <span class="help-block "><strong>{{ $message }}</strong></span>
                @endforeach
            @endif
        </div>
        <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
            <label for="email">Email address</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}">
            @if($errors->has('email'))
                @foreach ($errors->get('email') as $message)
                    <span class="help-block "><strong>{{ $message }}</strong></span>
                @endforeach
            @endif
        </div>
        <div class="form-group @if($errors->has('password')) {{ 'has-error' }} @endif">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="{{ old('password') }}">
            @if($errors->has('password'))
                @foreach ($errors->get('password') as $message)
                    <span class="help-block "><strong>{{ $message }}</strong></span>
                @endforeach
            @endif
        </div>
        <div class="form-group @if($errors->has('password_confirmation')) {{ 'has-error' }} @endif">
            <label for="password_confirmation">Password Confirm</label>
            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirm">
            @if($errors->has('password_confirmation'))
                @foreach ($errors->get('password_confirmation') as $message)
                    <span class="help-block "><strong>{{ $message }}</strong></span>
                @endforeach
            @endif
        </div>


        @if($errors->has('tos')) <div class="has-error"> @endif
        <div class="checkbox ">
            <label>
                <input type="checkbox" name="tos"> I Agree with TOS.
            </label>
        </div>
        @if($errors->has('tos')) </div> @endif
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
@endsection