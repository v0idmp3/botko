@extends('frontend.layout')


@section('content')
    <h1>Login form:</h1>
    @if(Session::has('message'))
        <div class="alert {!! Session::get('alert-class') !!}">
            <strong>{!! Session::get('message') !!}</strong>
        </div>
    @endif
    <form method="post" action="{{ URL::to('login') }}">
        {{ csrf_field() }}
        <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
            <label for="email">Email address</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}">
            @if($errors->has('email'))
                @foreach ($errors->get('email') as $message)
                    <span class="help-block "><strong>{{ $message }}</strong></span>
                @endforeach
            @endif
        </div>
        <div class="form-group @if($errors->has('password')) {{ 'has-error' }} @endif">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="{{ old('password') }}">
            @if($errors->has('password'))
                @foreach ($errors->get('password') as $message)
                    <span class="help-block "><strong>{{ $message }}</strong></span>
                @endforeach
            @endif
        </div>

        <div class="checkbox ">
            <label>
                <input type="checkbox" name="remember_me"> Remember me
            </label>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>


    </form>
@endsection