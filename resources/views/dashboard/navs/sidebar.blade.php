<!-- Main sidebar -->
<div class="sidebar sidebar-main sidebar-fixed">
    <div class="sidebar-content">
        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="{{ URL::asset('images/default-avatar.png') }}" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">{{ Auth::user()->name }}</span>                        
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="#"><i class="icon-cog3"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li><a href="{{ URL::to('dashboard') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    <li>
                        <a href="{{ URL::to('dashboard/bots') }}"><i class="icon-stack2"></i> <span>Bots</span></a>                        
                    </li>
                    <li>
                        <a href="{{ URL::to('dashboard/bots/my') }}"><i class="icon-stack2"></i> <span>My Bots</span></a>                        
                    </li>
                    <li>
                        <a href="{{ URL::to('dashboard/apibots') }}"><i class="icon-copy"></i> <span>API Bots</span></a>                        
                    </li>
                    <li>
                        <a href="#"><i class="icon-droplet2"></i> <span>Platforms</span></a>
                        <ul>
                            <li><a href="{{ URL::to('dashboard/platform/facebook') }}">Facebook</a></li>
                            <li><a href="{{ URL::to('dashboard/platform/telegram') }}">Telegram</a></li>                            
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->