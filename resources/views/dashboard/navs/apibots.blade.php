@if(sizeof($bots) == 0)
<div class="panel panel-flat">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                You dont have any API bot
            </div>
        </div>        
    </div>
</div>
@else
<div class="panel panel-flat">
   
        <div class="row">
            <div class="col-lg-12">
                <ul>
                    @foreach($bots as $bot)
                    <li class="list-group-item">
                        <a href='{{ URL::to("dashboard/apibots/edit/".$bot->id) }}'>{{ $bot->name }}</a>                        
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
 
</div>

@endif