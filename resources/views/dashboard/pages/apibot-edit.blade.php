@extends('dashboard.layout')


@section('content')
<?php
$data = json_decode($bot->data, true);
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(".add-param").on('click', function (e) {
            e.preventDefault();
            var clone = $(".params-container").children().clone(true, true);
            $('.cloned-params').append(clone);
            $(".cloned-params .params-group").last().find("input[type='text']").val("");
            $(".cloned-params .remove-param").on('click', function (e) {
                e.preventDefault();
                $(this).parent().parent().remove();
            });
        });
        $(".successAlert").fadeOut('slow', function () {
            $(this).remove();
        });
        $(".cloned-params .remove-param").on('click', function (e) {
            e.preventDefault();
            $(this).parent().parent().remove();
        });
    });
</script>
<div class="content">
    <div class="row">
        <div class="col-lg-2">            
            @include('dashboard.navs.apibots')            
        </div>
        <div class="col-lg-10">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Edit {{ $bot->name }}</h2>
                        <div class="heading-elements">
                            <a class="btn btn-danger" href='{{ URL::to("dashboard/apibots/delete/".$bot->id) }}'>Delete</a>
                        </div>   
                </div>
                <div class="panel-body">

                    @if(session('success'))
                    <div class="alert alert-success successAlert">
                        Your bot is changed successful :)
                    </div>
                    @endif                            
                    <form method="post" class="form-horizontal" action="{{ URL::to('dashboard/apibots/edit/'.$bot->id) }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value=""/>
                        <fieldset class="content-group">
                            <legend class="text-bold">Basic info</legend>
                            <div class="form-group @if($errors->has('name') || $errors->has('used_name')) {{ 'has-error' }} @endif">
                                <label for="name" class="control-label col-lg-2">Name</label>
                                <div class="col-lg-10">
                                    <input type="text" name="name" class="form-control " id="name" placeholder="Name" value="{{ old('name', $bot->name) }}">
                                    @if($errors->has('name'))
                                    @foreach ($errors->get('name') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                    @if($errors->has('used_name'))
                                    @foreach ($errors->get('used_name') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('keywords') || $errors->has('used_keyword')) {{ 'has-error' }} @endif">
                                <label for="keywords" class="control-label col-lg-2">Keywords (separate with withspace or comma)</label>
                                <div class="col-lg-10">
                                    <input type="text" name="keywords" class="form-control " id="keywords" placeholder="Keywords" value="{{ old('keywords', $bot->keywords) }}">
                                    @if($errors->has('keywords'))
                                    @foreach ($errors->get('keywords') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                    @if($errors->has('used_keyword'))
                                    @foreach ($errors->get('used_keyword') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('url')) {{ 'has-error' }} @endif">
                                <label for="url" class="control-label col-lg-2">Request URL</label>
                                <div class="col-lg-10">
                                    <input type="text" name="url" class="form-control " id="url" placeholder="Url" value="{{ old('url', $data['url']) }}">
                                    @if($errors->has('url'))
                                    @foreach ($errors->get('url') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('error')) {{ 'has-error' }} @endif">
                                <label for="error" class="control-label col-lg-2">Default Error</label>
                                <div class="col-lg-10">
                                    <textarea name="error" id="error" class="form-control">{{ old('error', $data['error']) }}</textarea>
                                    @if($errors->has('error'))
                                    @foreach ($errors->get('error') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="form-group @if($errors->has('request')) {{ 'has-error' }} @endif">
                                <label for="request" class="control-label col-lg-2">Request Type</label>
                                <div class="col-lg-10">
                                    <select name="request" class="form-control " id="request" >
                                        <option value="get" @if($data['type'] == "get") selected @endif>GET</option>
                                        <option value="post"@if($data['type'] == "post") selected @endif>POST</option>
                                    </select> 
                                    @if($errors->has('request'))
                                    @foreach ($errors->get('request') as $message)
                                    <span class="help-block "><strong>{{ $message }}</strong></span>
                                    @endforeach
                                    @endif
                                </div>                                
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <?php
                            $params = $data['params'];

                            if (count($params) > 0) {
                                $i = 0;
                                foreach ($params as $key => $value) {
                                    if ($i == 0) {
                                        $first[0] = $key;
                                        $first[1] = $value;
                                        $i++;
                                    } else {
                                        $rest[$key] = $value;
                                    }
                                }
                            }
                            ?>
                            <legend class="text-bold">Request Params</legend>
                            <div class="row params">
                                <div class="params-container">
                                    <div class="params-group">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Name</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="param_name[]" value="{{ $first[0] or '' }}" class="form-control" id="param_name[]"/>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Value</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="param_value[]" value="{{ $first[1] or '' }}" class="form-control" id="param_value[]"/>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-2 text-right removeButton">
                                            <a href="#" class="remove-param btn btn-danger right">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="cloned-params">
                                    @if(isset($rest))
                                    @foreach($rest as $key => $value)
                                    <div class="params-group">
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Name</label>
                                                <div class="col-lg-10">
                                                <input type="text" name="param_name[]" value="{{ $key }}" class="form-control" id="param_name[]"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="form-group">
                                                <label class="control-label col-lg-2">Value</label>
                                                <div class="col-lg-10">
                                                    <input type="text" name="param_value[]" value="{{ $value }}" class="form-control" id="param_value[]"/>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right removeButton">
                                            <a href="#" class="remove-param btn btn-danger right">Remove</a>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class='col-md-12'>
                                    <a href="#" class="add-param btn btn-success">Add more</a>
                                </div>
                                @if($errors->has('param_value'))
                                <div class='col-md-12'>
                                    <div class='alert alert-danger'>
                                        @foreach($errors->get('param_value') as $message)
                                        {{ $message }} <br>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">Additional info</legend>
                            <div class='form-group'>
                                <label for="description" class="control-label col-lg-2">Bot Description</label>
                                <div class="col-lg-10">
                                    <textarea name="description" id="description" class="form-control">{{ old('description', $bot->description) }}</textarea>
                                </div>                                
                            </div>
                            <div class='form-group'>
                                <label for="help" class="control-label col-lg-2">Bot Help</label>
                                <div class="col-lg-10">
                                    <textarea name="help" id="help" class="form-control">{{ old('help', $bot->help) }}</textarea>
                                </div>                                
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class='col-md-12'>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection