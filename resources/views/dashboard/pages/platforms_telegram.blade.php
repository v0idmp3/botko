@extends('dashboard.layout')


@section('content')
<script type='text/javascript'>
    $(document).ready(function () {
        $(".vt_submit").on('click', function () {
            var access_token = $("#access_token").val();
            $.ajax({
                url: '{{ URL::to("api/telegram/".Auth::user()->uuid."/save-token") }}',
                type: "post",
                data: {access_token: access_token},
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data.success) {
                        $("#saveMessage").removeClass('hidden');
                    } else {
                        $(".access_token").addClass(data.input);
                    }

                }
            })
        });
    }
    );
</script>
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h3 class="panel-title">Register Telegram Bot</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group access_token">
                        <label for="verify_token" class="control-label">Access Token:</label>
                        <input type="text" id="access_token" name="access_token" class="form-control" max="254" min="5" value="{{ $access_token or '' }}">
                    </div>

                    <button type='button' class='btn btn-lg btn-primary vt_submit'>
                        @if(isset($access_token))
                        Update
                        @else
                        Save
                        @endif
                    </button>

                    <div class="alert alert-success hidden" id="saveMessage">
                        <strong>Saved!</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection