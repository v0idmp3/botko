@extends('dashboard.layout')


@section('content')
<meta name="csrf-token" content="{{{ Session::token() }}}">
<script type="text/javascript" src="{{ URL::asset('js/dashboardBots.js') }}"></script>
<div class="content">
    <div class="row">        
        <div class="col-lg-12">
            <div class="panel panel-flat">
                <div class="panel panel-heading">
                    <h5 class="panel-title">Available Bots</h2>
                </div>
                <div class="panel-body availableBots">                    
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="help" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <h3>Keywords</h3>
                    <p class="modal-keywords"></p>
                    <hr>
                    <h3>Description</h3>
                    <p class="modal-description"></p>
                    <hr>
                    <h3>Help</h3>
                    <p class="modal-help"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection