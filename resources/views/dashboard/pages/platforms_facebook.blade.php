@extends('dashboard.layout')


@section('content')
<script type='text/javascript'>
    $(document).ready(function () {
        $(".vt_submit").on('click', function () {
            var token_value = $("#verify_token").val();
            var page_token = $("#page_token").val();
            $.ajax({
                url: '{{ URL::to("api/facebook/".Auth::user()->uuid."/save-token") }}',
                type: "post",
                data: {verify_token: token_value, page_token: page_token},
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data.success) {
                        $(".webhook").removeClass('hidden');
                        $("#verify_token").val(data.verify_token);
                        $("#page_token").val(data.page_token);
                    } else {
                        $(".verify_token").addClass(data.input);
                    }

                }
            })
        });
    }
    );
</script>
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Register Facebook Application</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group page_token">
                        <label for="page_token" class="control-label">Page Token:</label>
                        <input type="text" id="page_token" name="page_token" class="form-control" value="{{ $page_token or '' }}">
                    </div>
                    <div class="form-group verify_token">
                        <label for="verify_token" class="control-label">Verify Token:</label>
                        <input type="text" id="verify_token" name="verify_token" class="form-control" max="254" min="5" value="{{ $verify_token or '' }}">
                    </div>

                    <button type='button' class='btn btn-lg btn-primary vt_submit'>
                        @if(isset($page_token) && isset($verify_token))
                        Update
                        @else
                        Save
                        @endif
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default webhook @if(!isset($verify_token)) hidden @endif">
                <div class="panel-heading">
                    <h3 class="panel-title">Your webhook</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="verify_token">Webhook URL (copy this):</label>
                        <input type="text" class="form-control" max="254" value="{{ URL::to('/api/facebook/'.Auth::user()->uuid) }}" readonly="readonly">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection