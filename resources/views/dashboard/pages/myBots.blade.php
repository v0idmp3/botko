@extends('dashboard.layout')


@section('content')
<script type="text/javascript">
    $(document).ready(function () {
        loadBots();
        //add triggers
        $(".myBots").on('click', '.help', function () {
            var bot_id = $(this).data('bot_id');
            $.ajax({
                url: "/dashboard/bots/help",
                type: "post",
                data: {bot_id: bot_id},
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    $(".modal-title").html(obj.name);
                    $(".modal-keywords").html(obj.keywords);
                    $(".modal-description").html(obj.description);
                    $(".modal-help").html(obj.help);
                    $("#help").modal('show');

                }
            });
        });

        $(".myBots").on('click', '.remove', function () {
            var bot_id = $(this).data('bot_id');
            $.ajax({
                url: "/dashboard/bots/remove",
                type: "post",
                data: {bot_id: bot_id},
                success: function (data) {
                    $("." + bot_id).remove();
                }
            });
        });
    });

    function loadBots() {
        $.ajax({
            type: "post",
            url: "/dashboard/bots/renderBots",
            async: false,
            data: {type: 'registered'},
            success: function (data) {
                $(".myBots").html(data);
            }
        });
    }
</script>
<div class="content">
    <div class="row">
        <div class="col-lg-12">            
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">My Bots</h2>                          
                </div>
                <div class="panel-body myBots">                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="help" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <h3>Keywords</h3>
                <p class="modal-keywords"></p>
                <hr>
                <h3>Description</h3>
                <p class="modal-description"></p>
                <hr>
                <h3>Help</h3>
                <p class="modal-help"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection