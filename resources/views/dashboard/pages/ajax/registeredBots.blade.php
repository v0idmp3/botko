
@if(sizeof($registeredBots) > 0)
<?php $i = 0; ?>
<div class="row">
    @foreach($registeredBots as $bot)
    <div class="col-lg-3 {{$bot->id}}">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <strong>{{ $bot->name }}</strong>
            </div>
            <div class="panel-body">
                {{ $bot->description }}
            </div>
            <div class="panel-footer">
                <ul>
                    @if(Auth::user()->uuid == $bot->uuid)
                    <li><a href="{{ URL::to('/dashboard/apibots/edit/'.$bot->id) }}" class="edit btn btn-sm btn-success">Edit</a></li>
                    @endif    
                    <li><a href="#" class="help btn btn-sm btn-info" data-bot_id = "{{ $bot->id }}">Help</a></li>
                </ul>
                <ul class="pull-right">
                    <li><small><a href="#" class="remove btn btn-sm btn-danger" data-bot_id = "{{ $bot->id }}">Remove</a></small>
                </ul>
            </div>
        </div>
    </div>

    <?php $i++; ?>
    @if($i % 4 == 0 )
</div><div class="row">
    @endif
    @endforeach
    @endif
