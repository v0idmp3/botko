@if(sizeof($availableBots) > 0)
<?php $i = 0; ?>
<div class="row">
    @foreach($availableBots as $bot)
    <div class="col-lg-3 {{ $bot->id }}">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <strong>{{ $bot->name }}</strong>
            </div>
            <div class="panel-body">
                {{ $bot->description }}
            </div>
            <div class="panel-footer">
                <ul>
                    @if(Auth::user()->uuid == $bot->uuid)
                    <li><a href="{{ URL::to('/dashboard/apibots/edit/'.$bot->id) }}" class="edit btn btn-sm btn-info">Edit</a></li> 
                    @endif
                </ul>
                <ul class="pull-right">
                    <li><a href="#" class="add btn btn-sm btn-success" data-bot_id = "{{ $bot->id }}">Add</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php $i++; ?>
    @if($i % 4 == 0 )
</div><div class="row">
    @endif
    @endforeach
    @endif
