$(document).ready(function () {
    //load registered bots
    loadBots();
    
    //add triggers
    $(".registeredBots").on('click', '.help', function(){
        var bot_id = $(this).data('bot_id');
        $.ajax({
            url: "bots/help",
            type: "post",
            data: {bot_id: bot_id},
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                $(".modal-title").html(obj.name);
                $(".modal-keywords").html(obj.keywords);
                $(".modal-description").html(obj.description);
                $(".modal-help").html(obj.help);
                $("#help").modal('show');
                
            }
        });
    });        
    
    $(".availableBots").on('click', '.add', function(){
        var bot_id = $(this).data('bot_id');
        $.ajax({
            url: "bots/add",
            type: "post",
            data: {bot_id: bot_id},
            success: function (data) {      
                $("."+bot_id).remove();
            }
        });
    });
});

function loadBots() {
    $.ajax({
        type: "post",
        url: "bots/renderBots",
        data: {type: 'registered'},
        success: function (data) {
            $(".registeredBots").html(data);           
        }
    });
    $.ajax({
        type: "post",
        url: "bots/renderBots",
        data: {type: 'available'},
        success: function (data) {
            $(".availableBots").html(data);            
        }
    });
}